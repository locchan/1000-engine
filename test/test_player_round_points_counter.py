from engine.classes.card import Card
from engine.engine import count_player_round_points


def test_player_round_points_counter_1():
    assert count_player_round_points(
        [
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("TEN", None, None),
            Card("TEN", None, None),
            Card("TEN", None, None),
            Card("TEN", None, None),
            Card("JACK", None, None),
            Card("JACK", None, None),
            Card("JACK", None, None),
            Card("JACK", None, None),
            Card("QUEEN", None, None),
            Card("QUEEN", None, None),
            Card("QUEEN", None, None),
            Card("QUEEN", None, None),
            Card("KING", None, None),
            Card("KING", None, None),
            Card("KING", None, None),
            Card("KING", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
        ],
        [
            "SPADES",
            "HEARTS"
        ]
    ) == 280


def test_player_round_points_counter_2():
    assert count_player_round_points(
        [
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("TEN", None, None),
            Card("QUEEN", None, None),
            Card("KING", None, None),
            Card("KING", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
        ],
        [
            "HEARTS"
        ]
    ) == 155


def test_player_round_points_counter_3():
    assert count_player_round_points(
        [
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("TEN", None, None),
            Card("TEN", None, None),
            Card("TEN", None, None),
            Card("TEN", None, None),
            Card("JACK", None, None),
            Card("JACK", None, None),
            Card("JACK", None, None),
            Card("JACK", None, None),
            Card("QUEEN", None, None),
            Card("QUEEN", None, None),
            Card("QUEEN", None, None),
            Card("QUEEN", None, None),
            Card("KING", None, None),
            Card("KING", None, None),
            Card("KING", None, None),
            Card("KING", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
        ],
        [
            "ACE",
            "HEARTS"
        ]
    ) == 420


def test_player_round_points_counter_4():
    assert count_player_round_points(
        [
        ],
        [
        ]
    ) == 0


def test_player_round_points_counter_5():
    assert count_player_round_points(
        [
            Card("ACE", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
        ],
        [
        ]
    ) == 10


def test_player_round_points_counter_6():
    assert count_player_round_points(
        [
            Card("ACE", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
            Card("KING", None, None),
            Card("QUEEN", None, None),
            Card("NINE", None, None),
        ],
        [
            "CLUBS"
        ]
    ) == 60


def test_player_round_points_counter_7():
    assert count_player_round_points(
        [
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
            Card("KING", None, None),
            Card("QUEEN", None, None),
            Card("NINE", None, None),
        ],
        [
            "CLUBS",
            "ACE"
        ]
    ) == 290


def test_player_round_points_counter_8():
    assert count_player_round_points(
        [
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("ACE", None, None),
        ],
        [
        ]
    ) == 35
