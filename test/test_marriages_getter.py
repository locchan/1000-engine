from engine.classes.card import Card
from engine.engine import get_marriages_of_hand


def test_marriages_getter_1():
    assert list(get_marriages_of_hand(
        [
            Card("QUEEN", "SPADES", None),
            Card("NINE", "HEARTS", None),
            Card("NINE", "CLUBS", None),
            Card("KING", "SPADES", None),
            Card("NINE", "DIAMONDS", None),
            Card("NINE", "CLUBS", None),
            Card("QUEEN", "DIAMONDS", None),
        ]
    )) == ["SPADES"]


def test_marriages_getter_2():
    assert list(get_marriages_of_hand(
        [
            Card("QUEEN", "SPADES", None),
            Card("NINE", "HEARTS", None),
            Card("NINE", "CLUBS", None),
            Card("KING", "SPADES", None),
            Card("NINE", "DIAMONDS", None),
            Card("KING", "DIAMONDS", None),
            Card("QUEEN", "DIAMONDS", None),
        ]
    )) == ["SPADES", "DIAMONDS"]


def test_marriages_getter_3():
    assert list(get_marriages_of_hand(
        [
            Card("ACE", "SPADES", None),
            Card("ACE", "HEARTS", None),
            Card("ACE", "CLUBS", None),
            Card("KING", "SPADES", None),
            Card("NINE", "DIAMONDS", None),
            Card("ACE", "DIAMONDS", None),
            Card("QUEEN", "DIAMONDS", None),
        ]
    )) == ["ACE"]


def test_marriages_getter_4():
    assert list(get_marriages_of_hand(
        [
            Card("ACE", "SPADES", None),
            Card("ACE", "HEARTS", None),
            Card("ACE", "CLUBS", None),
            Card("KING", "SPADES", None),
            Card("NINE", "DIAMONDS", None),
            Card("TEN", "DIAMONDS", None),
            Card("QUEEN", "DIAMONDS", None),
        ]
    )) == []


def test_marriages_getter_5():
    assert list(get_marriages_of_hand(
        [
            Card("QUEEN", "SPADES", None),
            Card("QUEEN", "HEARTS", None),
            Card("QUEEN", "CLUBS", None),
            Card("KING", "SPADES", None),
            Card("QUEEN", "DIAMONDS", None),
            Card("KING", "CLUBS", None),
            Card("KING", "HEARTS", None),
            Card("KING", "DIAMONDS", None),
            Card("ACE", "SPADES", None),
            Card("ACE", "HEARTS", None),
            Card("ACE", "CLUBS", None),
            Card("ACE", "DIAMONDS", None)
        ]
    )) == ["SPADES", "HEARTS", "CLUBS", "DIAMONDS", "ACE"]


def test_marriages_getter_6():
    assert list(get_marriages_of_hand( [
        Card("QUEEN", "DIAMONDS", None),
        Card("KING", "CLUBS", None)
    ])) == []


def test_marriages_getter_7():
    assert list(get_marriages_of_hand(
        [
            Card("JACK", "SPADES", None),
            Card("ACE", "DIAMONDS", None),
            Card("KING", "SPADES", None),
            Card("TEN", "SPADES", None),
            Card("ACE", "CLUBS", None),
            Card("TEN", "HEARTS", None),
        ]
    )) == []
