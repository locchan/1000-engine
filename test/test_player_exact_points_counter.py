from engine.classes.card import Card
from engine.engine import count_exact_round_points


def test_player_exact_points_counter_1():
    assert count_exact_round_points(
        [
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("TEN", None, None),
            Card("TEN", None, None),
            Card("TEN", None, None),
            Card("TEN", None, None),
            Card("JACK", None, None),
            Card("JACK", None, None),
            Card("JACK", None, None),
            Card("JACK", None, None),
            Card("QUEEN", None, None),
            Card("QUEEN", None, None),
            Card("QUEEN", None, None),
            Card("QUEEN", None, None),
            Card("KING", None, None),
            Card("KING", None, None),
            Card("KING", None, None),
            Card("KING", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
        ],
        [
            "SPADES",
            "HEARTS"
        ]
    ) == 280


def test_player_exact_points_counter_2():
    assert count_exact_round_points(
        [
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("TEN", None, None),
            Card("QUEEN", None, None),
            Card("KING", None, None),
            Card("KING", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
        ],
        [
            "HEARTS"
        ]
    ) == 154


def test_player_exact_points_counter_3():
    assert count_exact_round_points(
        [
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("TEN", None, None),
            Card("TEN", None, None),
            Card("TEN", None, None),
            Card("TEN", None, None),
            Card("JACK", None, None),
            Card("JACK", None, None),
            Card("JACK", None, None),
            Card("JACK", None, None),
            Card("QUEEN", None, None),
            Card("QUEEN", None, None),
            Card("QUEEN", None, None),
            Card("QUEEN", None, None),
            Card("KING", None, None),
            Card("KING", None, None),
            Card("KING", None, None),
            Card("KING", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
        ],
        [
            "ACE",
            "HEARTS"
        ]
    ) == 420


def test_player_exact_points_counter_4():
    assert count_exact_round_points(
        [
        ],
        [
        ]
    ) == 0


def test_player_exact_points_counter_5():
    assert count_exact_round_points(
        [
            Card("ACE", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
        ],
        [
        ]
    ) == 11


def test_player_exact_points_counter_6():
    assert count_exact_round_points(
        [
            Card("ACE", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
            Card("KING", None, None),
            Card("QUEEN", None, None),
            Card("NINE", None, None),
        ],
        [
            "CLUBS"
        ]
    ) == 58


def test_player_exact_points_counter_7():
    assert count_exact_round_points(
        [
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
            Card("KING", None, None),
            Card("QUEEN", None, None),
            Card("NINE", None, None),
        ],
        [
            "CLUBS",
            "ACE"
        ]
    ) == 291
