from engine.checkers import can_put_card, check_if_this_card_creates_a_trump
from engine.classes.card import Card
from engine.classes.player import Player
from engine.engine import create_deck, create_shuffled_deck, shuffle_deck
from engine.game.bidding import create_cycle_starting_with_player_after_100
from engine.game.classes.game import Game


def test_deck_creation():
    deck = create_deck()
    assert len(list(deck)) == 24


def test_shuffled_deck_creation():
    deck = create_shuffled_deck()
    assert len(list(deck)) == 24


def test_if_deck_is_shuffled():
    deck = list(create_deck())
    deck_s = shuffle_deck(deck)
    assert not deck == deck_s


def test_if_bid_cycle_is_valid_1():
    zhora = Player("Zhora", on_100=True)
    petya = Player("Petya")
    ivan = Player("Ivan")
    players = [
        zhora,
        petya,
        ivan
    ]
    cycle = create_cycle_starting_with_player_after_100(players)
    assert next(cycle) == petya  # First player to bid
    assert next(cycle) == ivan
    assert next(cycle) == zhora
    assert next(cycle) == petya  # Full circle


def test_if_bid_cycle_is_valid_2():
    zhora = Player("Zhora")
    petya = Player("Petya", on_100=True)
    ivan = Player("Ivan")
    players = [
        zhora,
        petya,
        ivan
    ]
    cycle = create_cycle_starting_with_player_after_100(players)
    assert next(cycle) == ivan  # First player to bid
    assert next(cycle) == zhora
    assert next(cycle) == petya
    assert next(cycle) == ivan  # Full circle


def test_if_bid_cycle_is_valid_3():
    zhora = Player("Zhora")
    petya = Player("Petya")
    ivan = Player("Ivan", on_100=True)
    players = [
        zhora,
        petya,
        ivan
    ]
    cycle = create_cycle_starting_with_player_after_100(players)
    assert next(cycle) == zhora  # First player to bid
    assert next(cycle) == petya
    assert next(cycle) == ivan
    assert next(cycle) == zhora  # Full circle


def test_if_player_can_put_card_1():
    hand = [
        Card("TEN", "DIAMONDS", None),
        Card("ACE", "HEARTS", None)
    ]
    table_zone = [
            Card("NINE", "DIAMONDS", None),
            Card("ACE", "HEARTS", None)
    ]
    assert not can_put_card(hand, Card("ACE", "HEARTS", None), table_zone)


def test_if_player_can_put_card_2():
    hand = [
        Card("TEN", "DIAMONDS", None),
        Card("ACE", "HEARTS", None)
    ]
    table_zone = [
            Card("NINE", "DIAMONDS", None),
            Card("ACE", "HEARTS", None)
    ]
    assert can_put_card(hand, Card("TEN", "DIAMONDS", None), table_zone)


def test_if_player_can_put_card_3():
    hand = [
        Card("TEN", "DIAMONDS", None),
        Card("ACE", "HEARTS", None)
    ]
    table_zone = [
    ]
    assert can_put_card(hand, Card("TEN", "DIAMONDS", None), table_zone)


def test_if_player_created_trump_1():
    hand = [
        Card("QUEEN", "DIAMONDS", None),
        Card("KING", "DIAMONDS", None)
    ]
    assert check_if_this_card_creates_a_trump(Card("QUEEN", "DIAMONDS", None), hand) == "DIAMONDS"


def test_if_player_created_trump_2():
    hand = [
        Card("QUEEN", "DIAMONDS", None),
        Card("KING", "CLUBS", None)
    ]
    assert check_if_this_card_creates_a_trump(Card("QUEEN", "DIAMONDS", None), hand) is None


def test_if_player_created_trump_3():
    hand = [
        Card("ACE", "DIAMONDS", None),
        Card("ACE", "CLUBS", None),
        Card("ACE", "SPADES", None),
        Card("ACE", "HEARTS", None)
    ]
    assert check_if_this_card_creates_a_trump(Card("ACE", "DIAMONDS", None), hand) == "ACE"
