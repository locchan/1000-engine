from engine.checkers import check_buyin_validity
from engine.classes.card import Card


def test_buyin_validator_1():
    assert not check_buyin_validity(
        [
            Card("NINE", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None)
        ]
    )


def test_buyin_validator_2():
    assert not check_buyin_validity(
        [
            Card("NINE", None, None),
            Card("JACK", None, None),
            Card("NINE", None, None)
        ]
    )


def test_buyin_validator_3():
    assert check_buyin_validity(
        [
            Card("NINE", None, None),
            Card("ACE", None, None),
            Card("JACK", None, None)
        ]
    )


def test_buyin_validator_4():
    assert check_buyin_validity(
        [
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("ACE", None, None)
        ]
    )


def test_buyin_validator_5():
    assert check_buyin_validity(
        [
            Card("JACK", None, None),
            Card("JACK", None, None),
            Card("ACE", None, None)
        ]
    )


def test_buyin_validator_6():
    assert check_buyin_validity(
        [
            Card("JACK", None, None),
            Card("NINE", None, None),
            Card("ACE", None, None)
        ]
    )


def test_buyin_validator_7():
    assert not check_buyin_validity(
        [
            Card("NINE", None, None),
            Card("JACK", None, None),
            Card("JACK", None, None)
        ]
    )
