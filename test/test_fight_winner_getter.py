from engine.classes.card import Card
from engine.checkers import get_winner_of_fight
from engine.classes.player import Player


zhora = Player("Zhora")
ivan = Player("Ivan")
petya = Player("Petya")


def test_fight_winner_getter_1():
    assert get_winner_of_fight(
        [
            Card("NINE", "HEARTS", zhora),
            Card("TEN", "HEARTS", ivan),
            Card("NINE", "CLUBS", petya)
        ], None
    ) == ivan


def test_fight_winner_getter_2():
    assert get_winner_of_fight(
        [
            Card("NINE", "DIAMONDS", zhora),
            Card("TEN", "HEARTS", ivan),
            Card("NINE", "CLUBS", petya)
        ], None
    ) == zhora


def test_fight_winner_getter_3():
    assert get_winner_of_fight(
        [
            Card("NINE", "DIAMONDS", zhora),
            Card("TEN", "HEARTS", ivan),
            Card("NINE", "CLUBS", petya)
        ], "CLUBS"
    ) == petya


def test_fight_winner_getter_4():
    assert get_winner_of_fight(
        [
            Card("NINE", "DIAMONDS", zhora),
            Card("ACE", "HEARTS", ivan),
            Card("NINE", "CLUBS", petya)
        ], "HEARTS"
    ) == ivan


def test_fight_winner_getter_5():
    assert get_winner_of_fight(
        [
            Card("ACE", "DIAMONDS", zhora),
            Card("ACE", "HEARTS", ivan),
            Card("ACE", "CLUBS", petya)
        ], None
    ) == zhora
