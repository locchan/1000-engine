from engine.classes.player import Player
from engine.engine import add_points_to_player


# 555 test
def test_player_points_adder_1():
    zhora = Player("Zhora", points=400, on_barrel=False, barrel_rounds_played=0, on_bid=False, barrels_fallen=0)
    add_points_to_player(zhora, 155)
    assert zhora.points == 0


# Barrel climb test
def test_player_points_adder_2():
    zhora = Player("Zhora", points=800, on_barrel=False, barrel_rounds_played=0, on_bid=False, barrels_fallen=0)
    add_points_to_player(zhora, 155)
    assert zhora.points == 880 and zhora.on_barrel


# Barrel climb with over 1000 and not on barrel test
def test_player_points_adder_3():
    zhora = Player("Zhora", points=870, on_barrel=False, barrel_rounds_played=0, on_bid=False, barrels_fallen=0)
    add_points_to_player(zhora, 500)
    assert zhora.points == 880 and zhora.on_barrel


# Barrel fall test
def test_player_points_adder_4():
    zhora = Player("Zhora", points=880, on_barrel=True, barrel_rounds_played=2, on_bid=False, barrels_fallen=0)
    add_points_to_player(zhora, 500)
    assert zhora.points == 680 and not zhora.on_barrel


# -555 test
def test_player_points_adder_5():
    zhora = Player("Zhora", points=-400, on_barrel=False, barrel_rounds_played=0, on_bid=False, barrels_fallen=0)
    add_points_to_player(zhora, -155)
    assert zhora.points == 0


# Trying to win without a bid test
def test_player_points_adder_6():
    zhora = Player("Zhora", points=880, on_barrel=True, barrel_rounds_played=0, on_bid=False, barrels_fallen=0)
    add_points_to_player(zhora, 160)
    assert zhora.points == 880 and zhora.on_barrel and not zhora.winner


# Winning test
def test_player_points_adder_7():
    zhora = Player("Zhora", points=880, on_barrel=True, barrel_rounds_played=0, on_bid=True,
                   barrels_fallen=0, bid_amount=120)
    add_points_to_player(zhora, 120)
    assert zhora.winner


# Falling from third barrel
def test_player_points_adder_8():
    zhora = Player("Zhora", points=880, on_barrel=True, barrel_rounds_played=2, on_bid=True,
                   barrels_fallen=2, bid_amount=120)
    add_points_to_player(zhora, 110)
    assert zhora.points == 0


# Not winning a bid
def test_player_points_adder_9():
    zhora = Player("Zhora", points=300, on_barrel=False, barrel_rounds_played=0, on_bid=True,
                   barrels_fallen=0, bid_amount=120)
    add_points_to_player(zhora, 110)
    assert zhora.points == 180


# Not winning a bid and falling to negative score
def test_player_points_adder_10():
    zhora = Player("Zhora", points=300, on_barrel=False, barrel_rounds_played=0, on_bid=True,
                   barrels_fallen=0, bid_amount=320)
    add_points_to_player(zhora, 110)
    assert zhora.points == -20


# Winning over 1000 without bid and getting on barrel
def test_player_points_adder_11():
    zhora = Player("Zhora", points=800, on_barrel=False, barrel_rounds_played=0, on_bid=True,
                   barrels_fallen=0, bid_amount=320)
    add_points_to_player(zhora, 320)
    assert zhora.points == 880 and zhora.on_barrel


# Getting well over bid
def test_player_points_adder_12():
    zhora = Player("Zhora", points=740, on_barrel=False, barrel_rounds_played=0, on_bid=True,
                   barrels_fallen=0, bid_amount=120)
    add_points_to_player(zhora, 320)
    assert zhora.points == 860 and not zhora.on_barrel


# Getting three bolts
def test_player_points_adder_13():
    zhora = Player("Zhora", bolts=2, points=270)
    add_points_to_player(zhora, 0)
    assert zhora.points == 150


# Getting three bolts then getting 555 and falling to zero
def test_player_points_adder_15():
    zhora = Player("Zhora", bolts=2, points=675)
    add_points_to_player(zhora, 0)
    assert zhora.points == 0
