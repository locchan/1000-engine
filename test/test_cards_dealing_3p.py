from engine.classes.player import Player
from engine.engine import give_shuffled_cards_to_players_and_fill_buyin

players = [
    Player("Zhora"),
    Player("Petya"),
    Player("Ivan")
]
buyin = []


def deal_cards():
    global players, buyin
    give_shuffled_cards_to_players_and_fill_buyin(players, buyin)


def flush_cards():
    global players, buyin
    for player in players:
        player.hand = []
    buyin = []


deal_cards()


def test_find_card_duplicates():
    global players, buyin
    cards_list = []
    for player in players:
        for card in player.hand:
            cards_list.append(str(card))
    for card in buyin:
        cards_list.append(str(card))
    assert not any(cards_list.count(x) > 1 for x in cards_list)


def test_validate_cards_by_player_and_buyin():
    global players, buyin
    for player in players:
        assert len(player.hand) == 7
    assert len(buyin) == 3
