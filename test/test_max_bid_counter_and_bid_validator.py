from engine.classes.card import Card
from engine.classes.player import Player
from engine.engine import count_max_bid, can_bid


def test_max_bid_counter_1():
    zhora = Player("Zhora",
                   hand=[
                       Card("QUEEN", "SPADES", None),
                       Card("NINE", "HEARTS", None),
                       Card("NINE", "CLUBS", None),
                       Card("KING", "SPADES", None),
                       Card("NINE", "DIAMONDS", None),
                       Card("NINE", "CLUBS", None),
                       Card("QUEEN", "DIAMONDS", None),
                   ])
    assert count_max_bid(zhora) == 180


def test_max_bid_counter_2():
    zhora = Player("Zhora",
                   hand=[
                       Card("QUEEN", "SPADES", None),
                       Card("NINE", "HEARTS", None),
                       Card("NINE", "CLUBS", None),
                       Card("KING", "DIAMONDS", None),
                       Card("NINE", "DIAMONDS", None),
                       Card("NINE", "CLUBS", None),
                       Card("QUEEN", "DIAMONDS", None),
                   ])
    assert count_max_bid(zhora) == 200


def test_max_bid_counter_3():
    zhora = Player("Zhora",
                   hand=[
                       Card("QUEEN", "HEARTS", None),
                       Card("NINE", "HEARTS", None),
                       Card("NINE", "CLUBS", None),
                       Card("KING", "CLUBS", None),
                       Card("NINE", "DIAMONDS", None),
                       Card("NINE", "CLUBS", None),
                       Card("QUEEN", "DIAMONDS", None),
                   ])
    assert count_max_bid(zhora) == 120


def test_max_bid_counter_4():
    zhora = Player("Zhora",
                   hand=[
                       Card("QUEEN", "HEARTS", None),
                       Card("NINE", "HEARTS", None),
                       Card("NINE", "CLUBS", None),
                       Card("KING", "CLUBS", None),
                       Card("NINE", "DIAMONDS", None),
                       Card("NINE", "CLUBS", None),
                       Card("QUEEN", "DIAMONDS", None),
                   ])
    assert count_max_bid(zhora) == 120


def test_max_bid_counter_5():
    zhora = Player("Zhora",
                   hand=[
                       Card("QUEEN", "HEARTS", None),
                       Card("NINE", "HEARTS", None),
                       Card("NINE", "CLUBS", None),
                       Card("KING", "CLUBS", None),
                       Card("NINE", "DIAMONDS", None),
                       Card("NINE", "CLUBS", None),
                       Card("QUEEN", "DIAMONDS", None),
                   ])
    assert not can_bid(zhora, 140)


def test_max_bid_counter_6():
    zhora = Player("Zhora",
                   hand=[
                       Card("QUEEN", "HEARTS", None),
                       Card("NINE", "HEARTS", None),
                       Card("NINE", "CLUBS", None),
                       Card("KING", "HEARTS", None),
                       Card("NINE", "DIAMONDS", None),
                       Card("NINE", "CLUBS", None),
                       Card("QUEEN", "DIAMONDS", None),
                   ])
    assert can_bid(zhora, 140)


def test_max_bid_counter_7():
    zhora = Player("Zhora",
                   hand=[
                       Card("QUEEN", "HEARTS", None),
                       Card("NINE", "HEARTS", None),
                       Card("NINE", "CLUBS", None),
                       Card("KING", "HEARTS", None),
                       Card("NINE", "DIAMONDS", None),
                       Card("NINE", "CLUBS", None),
                       Card("QUEEN", "DIAMONDS", None),
                   ])
    assert can_bid(zhora, 220)


def test_max_bid_counter_8():
    zhora = Player("Zhora",
                   hand=[
                       Card("QUEEN", "HEARTS", None),
                       Card("NINE", "HEARTS", None),
                       Card("NINE", "CLUBS", None),
                       Card("KING", "HEARTS", None),
                       Card("NINE", "DIAMONDS", None),
                       Card("NINE", "CLUBS", None),
                       Card("QUEEN", "DIAMONDS", None),
                   ])
    assert not can_bid(zhora, 240)
