from engine.classes.card import Card
from engine.engine import count_cards_price


def test_cards_price_counter_1():
    assert count_cards_price(
        [
            Card("NINE", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None)
        ]
    ) == 0


def test_cards_price_counter_2():
    assert count_cards_price(
        [
            Card("NINE", None, None),
            Card("JACK", None, None),
            Card("ACE", None, None)
        ]
    ) == 13


def test_cards_price_counter_3():
    assert count_cards_price(
        [
            Card("ACE", None, None),
            Card("QUEEN", None, None),
            Card("KING", None, None)
        ]
    ) == 18


def test_cards_price_counter_4():
    assert count_cards_price(
        [
            Card("JACK", None, None),
            Card("JACK", None, None),
            Card("QUEEN", None, None)
        ]
    ) == 7


def test_cards_price_counter_5():
    assert count_cards_price(
        [
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("ACE", None, None)
        ]
    ) == 33


def test_cards_price_counter_6():
    assert count_cards_price(
        [
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("ACE", None, None),
            Card("TEN", None, None),
            Card("TEN", None, None),
            Card("TEN", None, None),
            Card("TEN", None, None),
            Card("JACK", None, None),
            Card("JACK", None, None),
            Card("JACK", None, None),
            Card("JACK", None, None),
            Card("QUEEN", None, None),
            Card("QUEEN", None, None),
            Card("QUEEN", None, None),
            Card("QUEEN", None, None),
            Card("KING", None, None),
            Card("KING", None, None),
            Card("KING", None, None),
            Card("KING", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
            Card("NINE", None, None),
        ]
    ) == 120
