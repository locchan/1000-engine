import itertools
import operator
import sys
from random import shuffle

from engine.settings import Card
from engine.settings import ACE_MARRIAGE_POINTS, MARRIAGE_POINTS, SUITS, CARDS_POINTS, CARDS, BARRELS_TO_FALL_TO_ZERO, \
    BARREL_FALL_PENALTY, POINTS_TO_FALL_TO_ZERO, WIN_POINTS, BARREL_ROUNDS_TO_FALL, BOLTS_TO_GET_PENALTY
from engine.locales import locales


if sys.version_info[0] < 3:
    from future.moves import collections
else:
    import collections


def create_deck(game_locale):
    for suit_type in SUITS:
        for card_type in CARDS:
            card_localized_type = locales[game_locale]["CARDS"][CARDS.index(card_type)]
            card_localized_suit = locales[game_locale]["SUITS"][SUITS.index(suit_type)]
            card_price = CARDS_POINTS[CARDS.index(card_type)]
            suit_price = MARRIAGE_POINTS[SUITS.index(suit_type)]
            yield Card(card_type, suit_type, None, type_localized=card_localized_type,
                       suit_localized=card_localized_suit, price=card_price, suit_price=suit_price)


def shuffle_deck(deck):
    shuffle(deck)


def create_shuffled_deck(game_locale="EN"):
    deck = list(create_deck(game_locale=game_locale))
    shuffle_deck(deck)
    return deck


def round_points(number_to_round):
    base = 5
    return int(base * round(float(number_to_round) / base))


def count_player_round_points(player_discard, player_marriages):
    return round_points(count_exact_round_points(player_discard, player_marriages))


def get_marriages_of_hand(hand):
    ends = []
    for card in hand:
        if card.type == CARDS[3] or card.type == CARDS[4]:
            ends.append(card.suit)
        if card.type == CARDS[5]:
            ends.append(CARDS[5])
    counter = collections.Counter(ends)
    for item in counter:
        if (counter[item] == 2 and item != "ACE") or (counter[item] == 4 and item == "ACE"):
            yield item


def count_exact_round_points(player_discard, player_marriages):
    points = 0
    for marriage in player_marriages:
        if marriage == "ACE":
            points += ACE_MARRIAGE_POINTS
            continue
        points += MARRIAGE_POINTS[SUITS.index(marriage)]
    points += count_cards_price(player_discard)
    return points


def count_cards_price(cards):
    total = 0
    for card in cards:
        total += CARDS_POINTS[CARDS.index(card.type)]
    return total


def can_bid(player, bid):
    return bid <= count_max_bid(player)


def count_max_bid(player):
    max_bid = 120
    for trump in get_marriages_of_hand(player.hand):
        if trump != "ACE":
            max_bid += MARRIAGE_POINTS[SUITS.index(trump)]
        else:
            max_bid += ACE_MARRIAGE_POINTS
    return max_bid


def full_barrel_fall(player):
    player.points = 0
    player.on_barrel = False
    player.barrels_fallen = 0
    player.barrel_round = 0


def barrel_fall(player):
    player.barrels_fallen += 1
    if player.barrels_fallen == BARRELS_TO_FALL_TO_ZERO:
        full_barrel_fall(player)
        return
    player.on_barrel = False
    player.barrel_round = 0
    player.points -= BARREL_FALL_PENALTY


def points_after_bolts_penalty(player):
    if not player.on_barrel:
        points_after = player.points - 120
        player.bolts = 0
        return points_after
    else:
        barrel_fall(player)
        player.bolts = 0
        return 0


def add_points_to_player(player, points):
    points_before = player.points
    points_after = points_before
    if points == 0:
        player.bolts += 1
    if player.bolts == BOLTS_TO_GET_PENALTY:
        points_after = points_after_bolts_penalty(player)
    if not player.on_bid:
        points_after += points
    else:
        if points < player.bid_amount:
            points_after = points_before - player.bid_amount
        if points >= player.bid_amount:
            points_after = points_before + player.bid_amount
    if points_after in POINTS_TO_FALL_TO_ZERO:
        player.points = 0
        return
    if player.on_barrel:
        player.barrel_round += 1
        if points_after >= WIN_POINTS and player.on_bid:
            player.winner = True
            return
        if player.barrel_round == BARREL_ROUNDS_TO_FALL or points_after < 880:
            barrel_fall(player)
            return
    elif points_after >= 880:
        player.on_barrel = True
        player.points = 880
        return
    else:
        player.points = points_after


def give_shuffled_cards_to_players_and_fill_buyin(players, buyin, game_locale="EN"):
    deck = create_shuffled_deck(game_locale=game_locale)
    for _ in itertools.repeat(None, 3):
        buyin.append(deck.pop())
    buyin = sort_cards(buyin)
    for aplayer in players:
        for _ in itertools.repeat(None, 7):
            card = deck.pop()
            card.owner = aplayer
            aplayer.hand.append(card)
        aplayer.hand = sort_cards(aplayer.hand)


def sort_cards(cards):
    return sorted(cards, key=operator.attrgetter("suit_price", "price"), reverse=True)


def give_card_to_player_from_player(player_from, player_to, card):
    player_from.hand.remove(card)
    player_to.hand.append(card)
    player_from.hand = sort_cards(player_from.hand)
    player_to.hand = sort_cards(player_to.hand)


def give_cards_to_player(player, cards):
    for card in cards:
        card.owner = player
        player.hand.append(card)
        player.hand = sort_cards(player.hand)
    cards = []


def give_cards_to_players_discard(player, cards):
    for card in cards:
        card.owner = player
        player.discard.append(card)
    cards = []


def is_there_a_winner(players):
    for player in players:
        if player.winner:
            return True
    return False


def put_card(hand, table, card):
    hand.remove(card)
    table.append(card)
