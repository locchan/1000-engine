from engine.classes.card import Card

CARDS = ["NINE", "TEN", "JACK", "QUEEN", "KING", "ACE"]
SUITS = ["SPADES", "CLUBS", "DIAMONDS", "HEARTS"]
MARRIAGE_POINTS = [40, 60, 80, 100]
ACE_MARRIAGE_POINTS = 200
BUYIN_INVALIDITY_CARDS = [
    [Card(CARDS[0], None, None), Card(CARDS[0], None, None)]
]
CARDS_POINTS = [0, 10, 2, 3, 4, 11]
BUYIN_INVALIDITY_POINTS = 4
HAND_INVALIDITY_POINTS = 14
HAND_INVALIDITY_CARDS = [
    [Card(CARDS[0], None, None), Card(CARDS[0], None, None), Card(CARDS[0], None, None), Card(CARDS[0], None, None)],
]
HAND_INVALIDITY_CARDS_INITIAL = [
    [Card(CARDS[2], None, None), Card(CARDS[2], None, None), Card(CARDS[2], None, None), Card(CARDS[2], None, None)],
]
HAND_INVALIDITY_CARDS_INITIAL_ON_100 = [
    [Card(CARDS[0], None, None), Card(CARDS[0], None, None), Card(CARDS[0], None, None)],
]
WIN_POINTS = 1000
BARREL_POINTS = 880
BARREL_ROUNDS_TO_FALL = 3
BOLTS_TO_GET_PENALTY = 3
BOLTS_PENALTY = 120
BARREL_FALL_PENALTY = 200
BARRELS_TO_FALL_TO_ZERO = 3
POINTS_TO_FALL_TO_ZERO = [555, -555]
