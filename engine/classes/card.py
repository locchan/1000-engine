class Card:
    price = 0

    def __init__(self, type, suit, owner, suit_localized=None, type_localized=None, price=None, suit_price=None):
        self.type = type
        self.suit = suit
        self.owner = owner
        if suit_localized is None:
            self.suit_localized = suit
        else:
            self.suit_localized = suit_localized
        if type_localized is None:
            self.type_localized = type
        else:
            self.type_localized = type_localized
        self.price = price
        self.suit_price = suit_price

    def __str__(self):
        return "{} {}".format(self.type_localized, self.suit_localized)


