class Player:
    def __init__(self, username, points=0, on_barrel=False, barrel_rounds_played=0, on_bid=False, bid_amount=0,
                 barrels_fallen=0, winner=False, on_100=False, hand=None, can_request_redeal=False, bolts=0,
                 discard=None, marriages=None):
        if marriages is None:
            marriages = []
        if discard is None:
            discard = []
        if hand is None:
            hand = []
        self.username = username
        self.points = points
        self.on_barrel = on_barrel
        self.barrel_round = barrel_rounds_played
        self.barrels_fallen = barrels_fallen
        self.on_bid = on_bid
        self.bid_amount = bid_amount
        self.winner = winner
        self.on_100 = on_100
        self.hand = hand
        self.can_request_redeal = can_request_redeal
        self.bolts = bolts
        self.discard = discard
        self.marriages = marriages

    def __str__(self):
        return self.username

    def get_readable_hand(self):
        cards = []
        for card in self.hand:
            cards.append(str(card))
        return cards
