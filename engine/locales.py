locales = {
    "EN": {
        "CARDS": ["NINE", "TEN", "JACK", "QUEEN", "KING", "ACE"],
        "SUITS": ["SPADES", "CLUBS", "DIAMONDS", "HEARTS"],
        "MESSAGES": {
            "BIDDING_PHASE_SEPARATOR":    "=============BIDDING=============",
            "DISCARDING_PHASE_SEPARATOR": "============DISCARDING===========",
            "ROUND_SEPARATOR":            "==============ROUND==============",
            "FIGHT_SEPARATOR":            "==============FIGHT==============",
            "CARDS_ON_THE_TABLE": "Cards on the table: ",
            "FIGHT_NUMBER": "Fight #",
            "ROUND_NUMBER": "Round #",
            "REQUESTED_REDEAL": "Requested redeal.",
            "PLAYER": "Player: ",
            "CANNOT_BID": "You cannot bid this amount!",
            "FOLD": "Folded!",
            "PLAYER_WON_BIDS": "Bidding winner is: ",
            "REGISTERING_USERS": "Registering users...",
            "REGISTERED_PLAYERS_LIST": "Registered players: ",
            "CURRENT_BID": "Current bid is: ",
            "CANNOT_PUT_CARD": "You cannot put this card.",
            "TRUMP_CREATED": "Has created a new trump: ",
            "WRONG_INPUT": "Wrong input. ",
            "TRY_AGAIN": "Please, try again. ",
            "WINS_THE_FIGHT": "Wins the fight.",
            "HAS_PUT_A_CARD": "Has put a card: ",
            "SHOW_BUYIN": "Buy-in is: ",
            "DISCARD_A_CARD_CHOICE": "Choose cards to discard: ",
            "DISCARD_A_CARD": "Discards a card: ",
            "SHOW_ROUND_POINTS": "Has earned points: ",
            "SHOW_TOTAL_POINTS": "Now has points: "
        },
        "PROMPTS": {
            "ANY_STRING": {
                "ENTER_USERNAME": "Please, enter username: "
            },
            "Y/N": {
                "INCREMENT_BID": "Do you want to raise bid?: ",
                "CANNOT_BID": "You can request redeal. Do you want to?: ",
            },
            "NUMERIC": {
                "CHOOSE_CARD": "Choose a card to put: ",
                "ENTER_FINAL_BID": "Enter your final bid: "
            }
        },
        "YES": ["Y", "y", "yes"],
        "NO": ["N", "n", "no"],
    },

    "RU": {
        "CARDS": ["Девять", "Десять", "Валет", "Дама", "Король", "Туз"],
        "SUITS": ["Пика", "Креста", "Буба", "Чирва"],
        "MESSAGES": {
            "BIDDING_PHASE_SEPARATOR":    "==============СТАВКИ=============",
            "DISCARDING_PHASE_SEPARATOR": "===========РАЗДАЧА КАРТ==========",
            "ROUND_SEPARATOR":            "=============РАЗДАЧА=============",
            "FIGHT_SEPARATOR":            "===============ХОД===============",
            "CARDS_ON_THE_TABLE": "Карты на столе: ",
            "FIGHT_NUMBER": "Ход #",
            "ROUND_NUMBER": "Раунд #",
            "REQUESTED_REDEAL": "Потребовал перерасдачу.",
            "PLAYER": "Игрок: ",
            "CANNOT_BID": "Вы не можете поставить столько!",
            "FOLD": "Пасанул!",
            "PLAYER_WON_BIDS": "Ставки выиграл: ",
            "REGISTERING_USERS": "Регистрация игроков...",
            "REGISTERED_PLAYERS_LIST": "Зарегистрированы игроки: ",
            "CURRENT_BID": "Ставка на данный момент: ",
            "CANNOT_PUT_CARD": "Вы не можете полоходить этой картой.",
            "TRUMP_CREATED": "Захвалил: ",
            "WRONG_INPUT": "Некорректный ввод. ",
            "TRY_AGAIN": "Попробуйте ещё раз. ",
            "WINS_THE_FIGHT": "Выиграл ход.",
            "HAS_PUT_A_CARD": "Походил картой: ",
            "SHOW_BUYIN": "Прикуп: ",
            "DISCARD_A_CARD_CHOICE": "Раздайте карты: ",
            "DISCARD_A_CARD": "Отдаёт карту: ",
            "SHOW_ROUND_POINTS": "Набрал: ",
            "SHOW_TOTAL_POINTS": "Имеет очков: "
        },
        "PROMPTS": {
            "ANY_STRING": {
                "ENTER_USERNAME": "Введите имя: "
            },
            "Y/N": {
                "INCREMENT_BID": "Хотите повысить?: ",
                "CANNOT_BID": "Вы можете потребовать пересдачу. Потребовать?: ",
            },
            "NUMERIC": {
                "CHOOSE_CARD": "Выберите карту для хода: ",
                "ENTER_FINAL_BID": "Введите окончательную ставку: "
            }
        },
        "YES": ["Д", "д", "да"],
        "NO": ["Н", "н", "нет"],
    }
}
