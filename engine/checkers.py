from engine.engine import count_cards_price, get_marriages_of_hand
from engine.settings import BUYIN_INVALIDITY_CARDS, BUYIN_INVALIDITY_POINTS, HAND_INVALIDITY_CARDS_INITIAL, \
    HAND_INVALIDITY_CARDS_INITIAL_ON_100, HAND_INVALIDITY_CARDS, HAND_INVALIDITY_POINTS, CARDS, CARDS_POINTS


def do_cards_contain(cards, other_cards):
    cards_types_list = []
    other_cards_types_list = []
    found = 0
    for card in cards:
        cards_types_list.append(card.type)
    for card in other_cards:
        other_cards_types_list.append(card.type)
        for i, card_str in enumerate(cards_types_list):
            for j, card_list_str in enumerate(other_cards_types_list):
                if card_str == card_list_str:
                    found += 1
                if found >= len(other_cards):
                    return True
        return False


def check_buyin_validity(buyin):
    for rule in BUYIN_INVALIDITY_CARDS:
        if do_cards_contain(buyin, rule):
            return False
    if count_cards_price(buyin) <= BUYIN_INVALIDITY_POINTS:
        return False
    return True


def check_hand_validity(player, right_after_deal=False):
    hand_cards = player.hand
    if right_after_deal:
        for rule in HAND_INVALIDITY_CARDS_INITIAL:
            if do_cards_contain(hand_cards, rule):
                return False
        if player.on_100 and player.bid_amount == 100:
            for rule in HAND_INVALIDITY_CARDS_INITIAL_ON_100:
                if do_cards_contain(hand_cards, rule):
                    return False
    for rule in HAND_INVALIDITY_CARDS:
        if do_cards_contain(hand_cards, rule):
            return False
    if count_cards_price(hand_cards) <= HAND_INVALIDITY_POINTS:
        return False
    return True


def get_winner_of_fight(table_zone, trump_suit):
    best_card = []
    start_suit = table_zone[0].suit
    for card in table_zone:
        card_price = CARDS_POINTS[CARDS.index(card.type)]
        if card.suit == trump_suit:
            card_price += 100
        elif start_suit != card.suit:
            card_price = -1
        if not best_card:
            best_card = [card, card_price]
            continue
        if best_card[1] < card_price:
            best_card = [card, card_price]
    return best_card[0].owner


def can_put_card(hand, card_to_put, table_zone):
    if not table_zone:
        return True
    start_suit = table_zone[0].suit
    if card_to_put.suit == start_suit:
        return True
    else:
        for card in hand:
            if card.suit == start_suit:
                return False
        return True


def check_if_this_card_creates_a_trump(card, hand):
    marriages = list(get_marriages_of_hand(hand))
    if card.suit in marriages and (card.type == "QUEEN" or card.type == "KING"):
        return card.suit
    if ("ACE" in marriages) and card.type == "ACE":
        return "ACE"
    return None
