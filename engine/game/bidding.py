from itertools import cycle

from engine.engine import can_bid, give_cards_to_player, give_card_to_player_from_player
from engine.game.utils import get_localized_yesno_prompt_result, get_localized_message, circular_shift, \
    print_cards_enumerated, choose_a_card_to_put, get_localized_numeric_prompt_result


def create_cycle_starting_with_player_after_100(players_temp_list):
    for player in players_temp_list:
        if player.on_100:
            players_temp_list = circular_shift(players_temp_list, player)
    cycle_to_return = cycle(players_temp_list)
    next(cycle_to_return)
    return cycle_to_return


def set_final_bid(game, bid_winner):
    while "incorrect bid":
        bid = get_localized_numeric_prompt_result("ENTER_FINAL_BID", game.game_locale)
        if can_bid(bid_winner, bid) and bid >= bid_winner.bid_amount:
            bid_winner.bid_amount = bid
            return
        print(get_localized_message("CANNOT_BID", game.game_locale))


def cards_discard(players, who_discards, game):
    print(get_localized_message("DISCARDING_PHASE_SEPARATOR", game.game_locale))
    print(get_localized_message("PLAYER", game.game_locale) + str(who_discards))
    print(get_localized_message("DISCARD_A_CARD_CHOICE", game.game_locale))
    players_list = list(players)
    players_list.pop(players.index(who_discards))
    card = choose_a_card_to_put(who_discards, game.game_locale)
    print(get_localized_message("PLAYER", game.game_locale) + str(who_discards))
    print(get_localized_message("DISCARD_A_CARD", game.game_locale) + str(card))
    give_card_to_player_from_player(who_discards, players_list[0], card)
    card = choose_a_card_to_put(who_discards, game.game_locale)
    print(get_localized_message("PLAYER", game.game_locale) + str(who_discards))
    print(get_localized_message("DISCARD_A_CARD", game.game_locale) + str(card))
    give_card_to_player_from_player(who_discards, players_list[1], card)


def bidding(game):
    print(get_localized_message("BIDDING_PHASE_SEPARATOR", game.game_locale))
    players_temp_list = list(game.players)
    current_bid = 100
    players_cycle = create_cycle_starting_with_player_after_100(players_temp_list)
    while "bidding":
        if len(players_temp_list) == 1:
            break
        current_player = next(players_cycle)
        if current_player not in players_temp_list:
            continue
        print(get_localized_message("PLAYER", game.game_locale) + str(current_player))
        print(get_localized_message("CURRENT_BID", game.game_locale) + str(current_bid))
        if get_localized_yesno_prompt_result("INCREMENT_BID", game.game_locale):
            if can_bid(current_player, current_bid + 5):
                current_player.bid_amount = current_bid + 5
                current_bid = current_bid + 5
                continue
            else:
                print(get_localized_message("CANNOT_BID", game.game_locale))
                current_player.bid_amount = 0
                players_temp_list.remove(current_player)
                continue
        else:
            print(str(current_player) + " " + get_localized_message("FOLD", game.game_locale))
            current_player.bid_amount = 0
            players_temp_list.remove(current_player)
            continue
    max_bid = 0
    bid_winner = None
    for player in game.players:
        if player.bid_amount > max_bid:
            bid_winner = player
    if bid_winner.on_100 and bid_winner.bid_amount == 100:
        show_buyin = False
    else:
        show_buyin = True
    print(get_localized_message("CURRENT_BID", game.game_locale) + str(current_bid))
    print(get_localized_message("PLAYER_WON_BIDS", game.game_locale) + str(bid_winner))
    if show_buyin:
        print(get_localized_message("SHOW_BUYIN", game.game_locale))
        print_cards_enumerated(game.buyin)
        give_cards_to_player(bid_winner, game.buyin)
    else:
        give_cards_to_player(bid_winner, game.buyin)
    bid_winner.on_bid = True
    cards_discard(game.players, bid_winner, game)
    for player in game.players:
        if player.can_request_redeal:
            print(get_localized_message("PLAYER", game.game_locale) + str(player))
            if get_localized_yesno_prompt_result("INCREMENT_BID", game.game_locale):
                print(get_localized_message("PLAYER", game.game_locale) + str(player))

    set_final_bid(game, bid_winner)
    return bid_winner

