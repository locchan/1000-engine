import itertools

from engine.classes.player import Player
from engine.game.utils import get_localized_message, get_localized_anystring_prompt_result


def initialize_a_game(game, game_type):
    init_game(game, game_type)


def register_player(game):
    game.players.append(Player(get_localized_anystring_prompt_result("ENTER_USERNAME", game.game_locale)))


def register_players(game, quantity):
    print(get_localized_message("REGISTERING_USERS", game.game_locale))
    for _ in itertools.repeat(None, quantity):
        register_player(game)


def print_players_list(game):
    print(get_localized_message("REGISTERED_PLAYERS_LIST", game.game_locale))
    for player in game.players:
        print(player)


def init_game_3p(game):
    register_players(game, 3)
    print_players_list(game)
    game.players[0].on_100 = True
    game.players[0].bid_amount = 100


def init_game(game, game_type):
    if game_type == "3p":
        init_game_3p(game)
