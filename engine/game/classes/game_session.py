import itertools

from engine.engine import is_there_a_winner, give_shuffled_cards_to_players_and_fill_buyin, add_points_to_player,\
    count_player_round_points
from engine.game.bidding import bidding
from engine.game.classes.game import Game
from engine.game.fight import Fight
from engine.game.initializing import initialize_a_game
from engine.game.utils import make_a_player_first_in_list, print_cards_enumerated, get_localized_message


class GameSession:

    def __init__(self, mode, locale):
        self.game = Game(mode, locale)

    def deal_cards(self):
        give_shuffled_cards_to_players_and_fill_buyin(self.game.players, self.game.buyin,
                                                      game_locale=self.game.game_locale)

    def start_round(self):
        self.game.round_number += 1
        self.deal_cards()
        for player in self.game.players:
            print(get_localized_message("PLAYER", self.game.game_locale) + str(player))
            print_cards_enumerated(player.hand)
        bidding_winner = bidding(self.game)
        self.game.players = make_a_player_first_in_list(bidding_winner, self.game.players)
        print(get_localized_message("ROUND_SEPARATOR", self.game.game_locale))
        print(get_localized_message("ROUND_NUMBER", self.game.game_locale) + str(self.game.round_number))
        for _ in itertools.repeat(None, 8):
            fight_winner = Fight(self.game).winner
            self.game.players = make_a_player_first_in_list(fight_winner, self.game.players)

    def after_round_cleanup(self):
        player_on_100_index = 0
        for player in self.game.players:
            if player.on_100:
                player_on_100_index = self.game.players.index(player)
            player.on_100 = False
            player.on_bid = False
            player.bid_amount = 0
        if player_on_100_index == len(self.game.players) - 1:
            self.game.players[0].on_100 = True
            self.game.players[0].bid_amount = 100
        else:
            self.game.players[player_on_100_index + 1].on_100 = True
        self.game.fight_number = 0
        self.game.current_trump = ""

    def start_game(self):
        initialize_a_game(self.game, self.game.game_type)
        while "there is no winner":
            self.start_round()
            for player in self.game.players:
                round_points = count_player_round_points(player.discard, player.marriages)
                print(get_localized_message("PLAYER", self.game.game_locale) + str(player))
                print(get_localized_message("SHOW_ROUND_POINTS", self.game.game_locale) + str(round_points))
                add_points_to_player(player, round_points)
                print(get_localized_message("SHOW_TOTAL_POINTS", self.game.game_locale) + str(player.points))
            if is_there_a_winner(self.game.players):
                break
            self.after_round_cleanup()


session = GameSession("3p", "RU")
session.start_game()
