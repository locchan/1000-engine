from engine.checkers import can_put_card, check_if_this_card_creates_a_trump, get_winner_of_fight
from engine.engine import put_card, give_cards_to_players_discard
from engine.game.utils import get_localized_message, choose_a_card_to_put, print_cards_enumerated


class Fight:
    winner = None

    def __init__(self, game):
        self.game = game
        self.game.fight_number += 1
        self.do_fight()

    def put_card_on_field(self, player):
        while "illegal card":
            card = choose_a_card_to_put(player, self.game.game_locale)
            if can_put_card(player.hand, card, self.game.table):
                print(get_localized_message("PLAYER", self.game.game_locale) + str(player))
                print(get_localized_message("HAS_PUT_A_CARD", self.game.game_locale) + str(card))
                if self.game.fight_number != 1:
                    if not self.game.table:
                        trump = check_if_this_card_creates_a_trump(card, player.hand)
                        if trump and check_if_this_card_creates_a_trump(card, player.hand) != self.game.current_trump:
                            self.game.current_trump = check_if_this_card_creates_a_trump(card, player.hand)
                            print(get_localized_message("PLAYER", self.game.game_locale) + str(player))
                            print(get_localized_message("TRUMP_CREATED", self.game.game_locale)
                                  + self.game.current_trump)
                            player.marriages.append(self.game.current_trump)
                put_card(player.hand, self.game.table, card)
                break
            else:
                print(get_localized_message("CANNOT_PUT_CARD", self.game.game_locale))

    def do_fight(self):
        print(get_localized_message("FIGHT_SEPARATOR", self.game.game_locale))
        print(get_localized_message("FIGHT_NUMBER", self.game.game_locale) + str(self.game.fight_number))
        for player in self.game.players:
            print(get_localized_message("PLAYER", self.game.game_locale) + str(player))
            self.put_card_on_field(player)
            print(get_localized_message("CARDS_ON_THE_TABLE", self.game.game_locale))
            print_cards_enumerated(self.game.table)
        winner = get_winner_of_fight(self.game.table, self.game.current_trump)
        print(get_localized_message("PLAYER", self.game.game_locale) + str(winner))
        print(get_localized_message("WINS_THE_FIGHT", self.game.game_locale))
        give_cards_to_players_discard(winner, self.game.table)
        self.game.table = []
        self.winner = winner
