import operator

from engine import locales


def get_localized_message(message, locale):
    return locales.locales[locale]["MESSAGES"][message]


def get_localized_anystring_prompt_result(prompt, locale):
    return input(locales.locales[locale]["PROMPTS"]["ANY_STRING"][prompt])


def get_localized_numeric_prompt_result(prompt, locale):
    while "wrong input":
        result = input(locales.locales[locale]["PROMPTS"]["NUMERIC"][prompt])
        if result.isdigit():
            return int(result)
        else:
            print(get_localized_message("WRONG_INPUT", locale))
            print(get_localized_message("TRY_AGAIN", locale))


def get_localized_yesno_prompt_result(prompt, locale):
    while "wrong input":
        input_result = input(locales.locales[locale]["PROMPTS"]["Y/N"][prompt])
        if input_result in locales.locales[locale]["YES"]:
            return True
        elif input_result in locales.locales[locale]["NO"]:
            return False
        else:
            print(get_localized_message("WRONG_INPUT", locale))
            print(get_localized_message("TRY_AGAIN", locale))


def circular_shift(list_to_shift, element):
    """
    Shifts a list starting with the specified element
    Example:
    Input:  [a,b,c] element = b; [a,b,c] element = a
    Output: [c,a,b]            ; [b,c,a]
    :param list_to_shift: a list in which the shift is performed
    :param element: element to start shift on
    :return: shifted list
    """
    return list_to_shift[list_to_shift.index(element)::] + list_to_shift[:list_to_shift.index(element):]


def print_cards_enumerated(cards):
    for num, card in enumerate(cards):
        print("{}. {}".format((num + 1), str(card)))


def choose_a_card_to_put(player, locale):
    print_cards_enumerated(player.hand)
    while "wrong card":
        chosen_card = get_localized_numeric_prompt_result("CHOOSE_CARD", locale)
        if (not chosen_card-1 > len(player.hand)) and (chosen_card-1 >= 0):
            if player.hand[chosen_card - 1]:
                return player.hand[chosen_card - 1]
        print(get_localized_message("WRONG_INPUT", locale))
        print(get_localized_message("TRY_AGAIN", locale))


def make_a_player_first_in_list(player, players):
    return circular_shift(players, player)
